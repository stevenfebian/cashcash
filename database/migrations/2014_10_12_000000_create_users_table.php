<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('username');
			$table->string('password');
			$table->string('name');
			$table->string('contact_no');
			$table->string('email')->unique();
            $table->string('status');
			$table->string('profile_image');
			$table->string('profile_summary');
            $table->string('auth_key');
            $table->rememberToken();
            $table->integer('is_admin')->default(0);
            $table->integer('status_id')->default(0);
            $table->timestamps();
        });
        Schema::create('user_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_status');
    }
}
