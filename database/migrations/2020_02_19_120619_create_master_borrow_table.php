<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterBorrowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrows', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->index();
            $table->date('due_date')->nullable()->index();
            $table->double('fine', 15, 2)->default(0);
            $table->integer('status_id')->default(0);
            $table->timestamps();
        });
        Schema::create('borrow_details', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('qty', 17, 2)->default(0);
            $table->integer('product_id')->default(0);
            $table->integer('bow_id')->default(0);
            $table->timestamps();
        });
        Schema::create('borrow_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrows');
        Schema::dropIfExists('borrow_details');
        Schema::dropIfExists('borrow_status');
    }
}
