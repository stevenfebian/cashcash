<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('title');
            $table->integer('author_id')->default(0);
            $table->integer('genre_id')->default(0);
            $table->longtext('notes')->nullable();
            $table->integer('status_id')->default(0);
            $table->timestamps();
        });
        Schema::create('product_details', function (Blueprint $table){
            $table->increments('id');
            $table->integer('product_id')->default(0);
            $table->decimal('stock', 17, 2)->default(0);
            $table->timestamps();

        });
        Schema::create('product_status', function (Blueprint $table){
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_details');
        Schema::dropIfExists('product_status');
    }
}
