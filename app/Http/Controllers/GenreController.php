<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $genres = Genre::all();
        return view('genre.index',  ['genre' => $genres]);
    }

    public function showpages()
    {
        //
		return view('genre/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
		return view('genre/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        Genre::create($request->all());
        return redirect()->route('genre.index')
                        ->with('success','Genre created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Genre  $genres
     * @return \Illuminate\Http\Response
     */
    public function show(Genre $genres)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Genre  $genres
     * @return \Illuminate\Http\Response
     */
    public function edit($genre)
    {
        $genres= Genre::find($genre->id);
        return view('genre.edit',compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $genres
     * @param  \App\Genre  $genres
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $genre)

    {
    //    $this->validate($request, [
    //         'page_name' => 'required',
    //         'page_title' => 'required',
	// 		'meta_title' => 'required',
    //     ]);
        Genre::find($genre)->update($request->all());
        return redirect()->route('genre.index')->with('success','Genre updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Genre  $genres
     * @return \Illuminate\Http\Response
     */
    public function destroy($genre)
    {
        Genre::find($genre)->delete();
        return redirect()->route('genre.index')
                        ->with('success','Genre deleted successfully');
    }

   
}
