<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $authors = Author::all();
        return view('author.index',  ['author' => $authors]);
    }

    public function showpages()
    {
        //
		return view('author/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
		return view('author/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        Author::create($request->all());
        return redirect()->route('author.index')
                        ->with('success','Author created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $authors
     * @return \Illuminate\Http\Response
     */
    public function show(Author $authors)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $authors
     * @return \Illuminate\Http\Response
     */
    public function edit($author_id)
    {
        $authors= Author::find($author_id);
        return view('author.edit',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $authors
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $author_id)

    {
    //    $this->validate($request, [
    //         'page_name' => 'required',
    //         'page_title' => 'required',
	// 		'meta_title' => 'required',
    //     ]);
        Author::find($author_id)->update($request->all());
        return redirect()->route('author.index')->with('success','Author updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $authors
     * @return \Illuminate\Http\Response
     */
    public function destroy($author_id)
    {
        Author::find($author_id)->delete();
        return redirect()->route('author.index')
                        ->with('success','Author deleted successfully');
    }

   
}
